package blocks.blessed;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;

/**
 * User: Orthus
 * Date: 10/1/13
 */
public class BlessedRock extends Block {

    public BlessedRock (int id, Material material)
    {
        super(id, material);
        setHardness(1.0F);
        setStepSound(Block.soundStoneFootstep);
        setUnlocalizedName("Blessed Stone");
        setCreativeTab(CreativeTabs.tabBlock);
        setTextureName("divine_equipment:Blessed_Stone");
    }
}

