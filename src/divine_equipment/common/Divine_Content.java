package common;

import blocks.blessed.BlessedRock;
import blocks.blessed.BlessedWood;
import blocks.tainted.TaintedRock;
import blocks.tainted.TaintedWood;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import divine_equipment.Divine_Equipment;
import items.Sigil;
import items.ender.EnderSword;
import items.tainted.TaintedAxe;
import items.blessed.*;
import items.tainted.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.EnumHelper;
import common.Config;

/**
 * User: Orthus
 * Date: 10/3/13
 */


public class Divine_Content {

    public static String modid = Divine_Equipment.modid;

    public static Config config;
    // Blocks
    public static Block blessedstone, blessedwood, taintedstone, taintedwood, BlessedAlter, TaintedAlter, EnderAlter, HiddenAlter;
    // Items
    public static Item BlankSigil, BlessedSigil, TaintedSigil, EnderSigil, HiddenSigil;
    public static Item TaintedDiamondSword, TaintedGoldSword, TaintedIronSword, TaintedStoneSword, TaintedWoodSword, TaintedDiamondPickaxe, TaintedGoldPickaxe, TaintedIronPickaxe, TaintedStonePickaxe, TaintedWoodPickaxe, TaintedDiamondAxe, TaintedGoldAxe, TaintedIronAxe, TaintedStoneAxe, TaintedWoodAxe, TaintedDiamondShovel, TaintedGoldShovel, TaintedIronShovel, TaintedStoneShovel, TaintedWoodShovel, TaintedDiamondHoe, TaintedGoldHoe, TaintedIronHoe, TaintedStoneHoe, TaintedWoodHoe;
    public static Item BlessedDiamondSword, BlessedGoldSword, BlessedIronSword, BlessedStoneSword, BlessedWoodSword, BlessedDiamondPickaxe, BlessedGoldPickaxe, BlessedIronPickaxe, BlessedStonePickaxe, BlessedWoodPickaxe, BlessedDiamondAxe, BlessedGoldAxe, BlessedIronAxe, BlessedStoneAxe, BlessedWoodAxe, BlessedDiamondShovel, BlessedGoldShovel, BlessedIronShovel, BlessedStoneShovel, BlessedWoodShovel, BlessedDiamondHoe, BlessedGoldHoe, BlessedIronHoe, BlessedStoneHoe, BlessedWoodHoe;
    public static Item EnderSword;
    // Init All materials
    EnumToolMaterial TaintedDiamondTool, TaintedGoldTool, TaintedIronTool, TaintedStoneTool, TaintedWoodTool, BlessedDiamondTool, BlessedGoldTool, BlessedIronTool, BlessedStoneTool, BlessedWoodTool;

    public Divine_Content()
    {
        registerBlocks();
        registerMaterials();
    }

    public void registerBlocks()
    {
    // Blessed Blocks
    blessedstone = new BlessedRock(config.BlessedStone, Material.rock).setUnlocalizedName("Blessed Stone").setTextureName("divine_equipment:BlessedStone");
    GameRegistry.registerBlock(blessedstone, modid+ blessedstone.getUnlocalizedName().substring(5));
    LanguageRegistry.addName(blessedstone, "Blessed Stone");
    blessedwood = new BlessedWood(config.BlessedWood, Material.wood).setUnlocalizedName("Blessed Wood").setTextureName("divine_equipment:BlessedWood");
    GameRegistry.registerBlock(blessedwood, modid+ blessedwood.getUnlocalizedName().substring(5));
    LanguageRegistry.addName(blessedwood, "Blessed Wood");
    // Tainted Blocks
    taintedstone = new TaintedRock(config.TaintedStone, Material.rock).setUnlocalizedName("Tainted Stone").setTextureName("divine_equipment:TaintedStone");
    GameRegistry.registerBlock(taintedstone, modid+ taintedstone.getUnlocalizedName().substring(5));
    LanguageRegistry.addName(taintedstone, "Tainted Stone");
    taintedwood = new TaintedWood(config.TaintedWood, Material.wood).setUnlocalizedName("Tainted Wood").setTextureName("divine_equipment:TaintedWood");
    GameRegistry.registerBlock(taintedwood, modid+ taintedwood.getUnlocalizedName().substring(5));
    LanguageRegistry.addName(taintedwood, "Tainted Wood");
    }


    public void registerMaterials()
    {
    // Tainted Tool Material Properties (Subject to Change)
    // Name, Harvest Level, Uses, Dig Speed, Damage, Enchantablity
       /*
        Default Values
        WOOD(0, 59, 2.0F, 0, 15),
        STONE(1, 131, 4.0F, 1, 5),
        IRON(2, 250, 6.0F, 2, 14),
        EMERALD(3, 1561, 8.0F, 3, 10),
        GOLD(0, 32, 12.0F, 0, 22);
         */
    TaintedDiamondTool = EnumHelper.addToolMaterial("Tainted Diamond", 3, 1003, 8.0F, 4, 15);
    TaintedGoldTool = EnumHelper.addToolMaterial("Tainted Gold", 2, 20, 12.0F, 1, 27);
    TaintedIronTool = EnumHelper.addToolMaterial("Tainted Iron", 2, 58, 8.0F, 3, 19);
    TaintedStoneTool = EnumHelper.addToolMaterial("Tainted Stone", 1, 88, 4.0F, 2, 10 );
    TaintedWoodTool = EnumHelper.addToolMaterial("Tainted Wood", 0, 40, 2.0F, 1, 20);

    // Blessed tool Material Properties (Subject to Change)
    BlessedDiamondTool = EnumHelper.addToolMaterial("Blessed Diamond", 3, 2850, 10.0F, 3, 22);
    BlessedGoldTool = EnumHelper.addToolMaterial("Blessed Gold", 2, 32, 14.00F, 0, 22);
    BlessedIronTool = EnumHelper.addToolMaterial("Blessed Iron", 2, 250, 8.0F, 2, 14);
    BlessedStoneTool = EnumHelper.addToolMaterial("Blessed Stone", 1, 131, 6.0F, 1, 5);
    BlessedWoodTool = EnumHelper.addToolMaterial("Blessed Wood", 0, 59, 4.0F, 0, 15);
    }
    // Ender Materials

    public void registerItems(){
    //Telling forge that we are creating stuff
        // Sigils

        BlankSigil = new Sigil(config.BlankSigil, "Blank_Sigil", "divine_equipment:Blank_Sigil");
        LanguageRegistry.addName(BlankSigil, "Blank Sigil");
        GameRegistry.addRecipe(new ItemStack(BlankSigil), " s ", "sgs", " s ", 's', Block.stone, 'g', Item.gunpowder);

        TaintedSigil = new Sigil(config.TaintedSigil, "Tainted_Sigil", "divine_equipment:Tainted_Sigil");
        LanguageRegistry.addName(TaintedSigil, "Tainted Sigil");
        GameRegistry.addRecipe(new ItemStack(TaintedSigil), "rrr", "rbr", "rrr", 'b', BlankSigil , 'r', Item.blazeRod);

        BlessedSigil = new Sigil(config.BlessedSigil, "Blessed_Sigil", "divine_equipment:Blessed_Sigil");
        LanguageRegistry.addName(BlessedSigil, "Blessed Sigil");
        GameRegistry.addRecipe(new ItemStack(BlessedSigil), "ggg", "gbg", "ggg", 'b', BlankSigil, 'g', Item.ingotGold);

        EnderSigil = new Sigil(config.EnderSigil, "Ender_Sigil", "divine_equipment:Ender_Sigil");
        LanguageRegistry.addName(EnderSigil, "Ender Sigil");
        GameRegistry.addRecipe(new ItemStack(EnderSigil), "eee", "ebe", "eee", 'b', BlankSigil , 'e', Item.eyeOfEnder);

        HiddenSigil = new Sigil(config.HiddenSigil, "Hidden_Sigil", "Hidden_Sigil");
        LanguageRegistry.addName(HiddenSigil, "Hidden Sigil");
        GameRegistry.addRecipe(new ItemStack(HiddenSigil), "beb", "ene", "tet", 'b', BlessedSigil, 'n', Item.netherStar, 'e', EnderSigil, 't', TaintedSigil );
    // Tainted Items
        // Tainted Swords
        TaintedDiamondSword = new TaintedSword(config.TaintedDiamondSword, TaintedDiamondTool).setUnlocalizedName("Tainted_Sword_Diamond").setTextureName("divine_equipment:Tainted_Sword_Diamond");
        LanguageRegistry.addName(TaintedDiamondSword, "Tainted Diamond Sword");
        GameRegistry.addRecipe(new ItemStack(TaintedDiamondSword), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Item.diamond, 's', Item.stick);

        TaintedGoldSword = new TaintedSword(config.TaintedGoldSword, TaintedGoldTool).setUnlocalizedName("Tainted_Sword_Gold").setTextureName("divine_equipment:Tainted_Sword_Gold");
        LanguageRegistry.addName(TaintedGoldSword, "Tainted Gold Sword");
        GameRegistry.addRecipe(new ItemStack(TaintedGoldSword), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Item.ingotGold, 's', Item.stick);

        TaintedIronSword = new TaintedSword(config.TaintedIronSword, TaintedIronTool).setUnlocalizedName("Tainted_Sword_Iron").setTextureName("divine_equipment:Tainted_Sword_Iron");
        LanguageRegistry.addName(TaintedIronSword, "Tainted Iron Sword");
        GameRegistry.addRecipe(new ItemStack(TaintedIronSword), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Item.ingotIron, 's', Item.stick);

        TaintedStoneSword = new TaintedSword(config.TaintedStoneSword, TaintedStoneTool).setUnlocalizedName("Tainted_Sword_Stone").setTextureName("divine_equipment:Tainted_Sword_Stone");
        LanguageRegistry.addName(TaintedStoneSword, "Tainted Stone Sword");
        GameRegistry.addRecipe(new ItemStack(TaintedStoneSword), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Block.stone, 's', Item.stick);

        TaintedWoodSword = new TaintedSword(config.TaintedWoodSword, TaintedWoodTool).setUnlocalizedName("Tainted_Sword_Wood").setTextureName("divine_equipment:Tainted_Sword_Wood");
        LanguageRegistry.addName(TaintedWoodSword, "Tainted Wooden Sword");
        GameRegistry.addRecipe(new ItemStack(TaintedWoodSword), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Block.planks, 's', Item.stick);

        // Pickaxes

        TaintedDiamondPickaxe = new TaintedPickaxe(config.TaintedDiamondPickaxe, TaintedDiamondTool).setUnlocalizedName("Tainted_Pickaxe_Diamond").setTextureName("divine_equipment:Tainted_Pickaxe_Diamond");
        LanguageRegistry.addName(TaintedDiamondPickaxe, "Tainted Diamond Pickaxe");
        GameRegistry.addRecipe(new ItemStack(TaintedDiamondPickaxe), "mSm", " s ", " s ", 'S', TaintedSigil, 'm', Item.diamond, 's', Item.stick);

        TaintedGoldPickaxe = new TaintedPickaxe(config.TaintedGoldPickaxe, TaintedGoldTool).setUnlocalizedName("Tainted_Pickaxe_Gold").setTextureName("divine_equipment:Tainted_Pickaxe_Gold");
        LanguageRegistry.addName(TaintedGoldPickaxe, "Tainted Gold Pickaxe");
        GameRegistry.addRecipe(new ItemStack(TaintedGoldPickaxe), "mSm", " s ", " s ", 'S', TaintedSigil, 'm', Item.ingotGold, 's', Item.stick);

        TaintedIronPickaxe = new TaintedPickaxe(config.TaintedIronPickaxe, TaintedIronTool).setUnlocalizedName("Tainted_Pickaxe_Iron").setTextureName("divine_equipment:Tainted_Pickaxe_Iron");
        LanguageRegistry.addName(TaintedIronPickaxe, "Tainted Iron Pickaxe");
        GameRegistry.addRecipe(new ItemStack(TaintedIronPickaxe), "mSm", " s ", " s ", 'S', TaintedSigil, 'm', Item.ingotIron, 's', Item.stick);

        TaintedStonePickaxe = new TaintedPickaxe(config.TaintedStonePickaxe, TaintedStoneTool).setUnlocalizedName("Tainted_Pickaxe_Stone").setTextureName("divine_equipment:Tainted_Pickaxe_Stone");
        LanguageRegistry.addName(TaintedStonePickaxe, "Tainted Stone Pickaxe");
        GameRegistry.addRecipe(new ItemStack(TaintedStonePickaxe), "mSm", " s ", " s ", 'S', TaintedSigil, 'm', Block.stone, 's', Item.stick);

        TaintedWoodPickaxe = new TaintedPickaxe(config.TaintedWoodPickaxe, TaintedWoodTool).setUnlocalizedName("Tainted_Pickaxe_Wood").setTextureName("divine_equipment:Tainted_Pickaxe_Wood");
        LanguageRegistry.addName(TaintedWoodPickaxe, "Tainted Wooden Pickaxe");
        GameRegistry.addRecipe(new ItemStack(TaintedWoodPickaxe), "mSm", " s ", " s ", 'S', TaintedSigil, 'm', Block.planks, 's', Item.stick);

        // Axes

        TaintedDiamondAxe = new TaintedAxe(config.TaintedDiamondAxe, TaintedDiamondTool).setUnlocalizedName("Tainted_Axe_Diamond").setTextureName("divine_equipment:Tainted_Axe_Diamond");
        LanguageRegistry.addName(TaintedDiamondAxe, "Tainted Diamond Axe");
        GameRegistry.addRecipe(new ItemStack(TaintedDiamondAxe), "mS ", "ms ", " s ", 'S', TaintedSigil, 'm', Item.diamond, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(TaintedDiamondAxe), " Sm", " sm", " s ", 'S', TaintedSigil, 'm', Item.diamond, 's', Item.stick);

        TaintedGoldAxe = new TaintedAxe(config.TaintedGoldAxe, TaintedGoldTool).setUnlocalizedName("Tainted_Axe_Gold").setTextureName("divine_equipment:Tainted_Axe_Gold");
        LanguageRegistry.addName(TaintedGoldAxe, "Tainted Gold Axe");
        GameRegistry.addRecipe(new ItemStack(TaintedGoldAxe), "mS ", "ms ", " s ", 'S', TaintedSigil, 'm', Item.ingotGold, 's', Item.stick);

        TaintedIronAxe = new TaintedAxe(config.TaintedIronAxe, TaintedIronTool).setUnlocalizedName("Tainted_Axe_Iron").setTextureName("divine_equipment:Tainted_Axe_Iron");
        LanguageRegistry.addName(TaintedIronAxe, "Tainted Iron Axe");
        GameRegistry.addRecipe(new ItemStack(TaintedIronAxe), "mS ", "ms ", " s ", 'S', TaintedSigil, 'm', Item.ingotIron, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(TaintedGoldAxe), " Sm", " sm", " s ", 'S', TaintedSigil, 'm', Item.ingotGold, 's', Item.stick);

        TaintedStoneAxe = new TaintedAxe(config.TaintedStoneAxe, TaintedStoneTool).setUnlocalizedName("Tainted_Axe_Stone").setTextureName("divine_equipment:Tainted_Axe_Stone");
        LanguageRegistry.addName(TaintedStoneAxe, "Tainted Stone Axe");
        GameRegistry.addRecipe(new ItemStack(TaintedStoneAxe), "mS ", "ms ", " s ", 'S', TaintedSigil, 'm', Block.stone, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(TaintedStoneAxe), " Sm", " sm", " s ", 'S', TaintedSigil, 'm', Block.stone, 's', Item.stick);

        TaintedWoodAxe = new TaintedAxe(config.TaintedWoodAxe, TaintedWoodTool).setUnlocalizedName("Tainted_Axe_Wood").setTextureName("divine_equipment:Tainted_Axe_Wood");
        LanguageRegistry.addName(TaintedWoodAxe, "Tainted Wooden Axe");
        GameRegistry.addRecipe(new ItemStack(TaintedWoodAxe), "mS ", "ms ", " s ", 'S', TaintedSigil, 'm', Block.planks, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(TaintedWoodAxe), " Sm", " sm", " s ", 'S', TaintedSigil, 'm', Block.planks, 's', Item.stick);

        // Shovels

        TaintedDiamondShovel = new TaintedShovel(config.TaintedDiamondShovel, TaintedDiamondTool).setUnlocalizedName("Tainted_Shovel_Diamond").setTextureName("divine_equipment:Tainted_Shovel_Diamond");
        LanguageRegistry.addName(TaintedDiamondShovel, "Tainted Diamond Shovel");
        GameRegistry.addRecipe(new ItemStack(TaintedDiamondShovel), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Item.diamond, 's', Item.stick);

        TaintedGoldShovel = new TaintedShovel(config.TaintedGoldShovel, TaintedGoldTool).setUnlocalizedName("Tainted_Shovel_Gold").setTextureName("divine_equipment:Tainted_Shovel_Gold");
        LanguageRegistry.addName(TaintedGoldShovel, "Tainted Gold Shovel");
        GameRegistry.addRecipe(new ItemStack(TaintedGoldShovel), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Item.ingotGold, 's', Item.stick);

        TaintedIronShovel = new TaintedShovel(config.TaintedIronShovel, TaintedIronTool).setUnlocalizedName("Tainted_Shovel_Iron").setTextureName("divine_equipment:Tainted_Shovel_Iron");
        LanguageRegistry.addName(TaintedIronShovel, "Tainted Iron Shovel");
        GameRegistry.addRecipe(new ItemStack(TaintedIronShovel), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Item.ingotIron, 's', Item.stick);

        TaintedStoneShovel = new TaintedShovel(config.TaintedStoneShovel, TaintedStoneTool).setUnlocalizedName("Tainted_Shovel_Stone").setTextureName("divine_equipment:Tainted_Shovel_Stone");
        LanguageRegistry.addName(TaintedStoneShovel, "Tainted Stone Shovel");
        GameRegistry.addRecipe(new ItemStack(TaintedStoneShovel), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Block.stone, 's', Item.stick);

        TaintedWoodShovel = new TaintedShovel(config.TaintedWoodShovel, TaintedWoodTool).setUnlocalizedName("Tainted_Shovel_Wood").setTextureName("divine_equipment:Tainted_Shovel_Wood");
        LanguageRegistry.addName(TaintedWoodShovel, "Tainted Wooden Shovel");
        GameRegistry.addRecipe(new ItemStack(TaintedWoodShovel), " m ", " S ", " s ", 'S', TaintedSigil, 'm', Block.planks, 's', Item.stick);

        // Tainted Hoes

        TaintedDiamondHoe = new TaintedHoe(config.TaintedDiamondHoe, TaintedDiamondTool).setUnlocalizedName("Tainted_Hoe_Diamond").setTextureName("divine_equipment:Tainted_Hoe_Diamond");
        LanguageRegistry.addName(TaintedDiamondHoe, "Tainted Diamond Hoe");
        GameRegistry.addRecipe(new ItemStack(TaintedDiamondHoe), "mS ", " s ", " s ", 'S', TaintedSigil, 'm', Item.diamond, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(TaintedDiamondHoe), " Sm", " s ", " s ", 'S', TaintedSigil, 'm', Item.diamond, 's', Item.stick);

        TaintedGoldHoe = new TaintedHoe(config.TaintedGoldHoe, TaintedGoldTool).setUnlocalizedName("Tainted_Hoe_Gold").setTextureName("divine_equipment:Tainted_Hoe_Gold");
        LanguageRegistry.addName(TaintedGoldHoe, "Tainted Gold Hoe");
        GameRegistry.addRecipe(new ItemStack(TaintedGoldHoe), "mS ", " s ", " s ", 'S', TaintedSigil, 'm', Item.ingotGold, 's', Item.stick);

        TaintedIronHoe = new TaintedHoe(config.TaintedIronHoe, TaintedIronTool).setUnlocalizedName("Tainted_Hoe_Iron").setTextureName("divine_equipment:Tainted_Hoe_Iron");
        LanguageRegistry.addName(TaintedIronHoe, "Tainted Iron Hoe");
        GameRegistry.addRecipe(new ItemStack(TaintedIronHoe), "mS ", " s ", " s ", 'S', TaintedSigil, 'm', Item.ingotIron, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(TaintedGoldHoe), " Sm", " s ", " s ", 'S', TaintedSigil, 'm', Item.ingotGold, 's', Item.stick);

        TaintedStoneHoe = new TaintedHoe(config.TaintedStoneHoe, TaintedStoneTool).setUnlocalizedName("Tainted_Hoe_Stone").setTextureName("divine_equipment:Tainted_Hoe_Stone");
        LanguageRegistry.addName(TaintedStoneHoe, "Tainted Stone Hoe");
        GameRegistry.addRecipe(new ItemStack(TaintedStoneHoe), "mS ", " s ", " s ", 'S', TaintedSigil, 'm', Block.stone, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(TaintedStoneHoe), " Sm", " s ", " s ", 'S', TaintedSigil, 'm', Block.stone, 's', Item.stick);

        TaintedWoodHoe = new TaintedHoe(config.TaintedWoodHoe, TaintedWoodTool).setUnlocalizedName("Tainted_Hoe_Wood").setTextureName("divine_equipment:Tainted_Hoe_Wood");
        LanguageRegistry.addName(TaintedWoodHoe, "Tainted Wooden Hoe");
        GameRegistry.addRecipe(new ItemStack(TaintedWoodHoe), "mS ", " s ", " s ", 'S', TaintedSigil, 'm', Block.planks, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(TaintedWoodHoe), " Sm", " s ", " s ", 'S', TaintedSigil, 'm', Block.planks, 's', Item.stick);

    // Blessed Items

        // Blessed Swords

        BlessedDiamondSword = new BlessedSword(config.BlessedDiamondSword, BlessedDiamondTool).setUnlocalizedName("Blessed_Sword_Diamond").setTextureName("divine_equipment:Blessed_Sword_Diamond");
        LanguageRegistry.addName(BlessedDiamondSword, "Blessed Diamond Sword");
        GameRegistry.addRecipe(new ItemStack(BlessedDiamondSword), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Item.diamond, 's', Item.stick);

        BlessedGoldSword = new BlessedSword(config.BlessedGoldSword, BlessedGoldTool).setUnlocalizedName("Blessed_Sword_Gold").setTextureName("divine_equipment:Blessed_Sword_Gold");
        LanguageRegistry.addName(BlessedGoldSword, "Blessed Gold Sword");
        GameRegistry.addRecipe(new ItemStack(BlessedGoldSword), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Item.ingotGold, 's', Item.stick);

        BlessedIronSword = new BlessedSword(config.BlessedIronSword, BlessedIronTool).setUnlocalizedName("Blessed_Sword_Iron").setTextureName("divine_equipment:Blessed_Sword_Iron");
        LanguageRegistry.addName(BlessedIronSword, "Blessed Iron Sword");
        GameRegistry.addRecipe(new ItemStack(BlessedIronSword), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Item.ingotIron, 's', Item.stick);

        BlessedStoneSword = new BlessedSword(config.BlessedStoneSword, BlessedStoneTool).setUnlocalizedName("Blessed_Sword_Stone").setTextureName("divine_equipment:Blessed_Sword_Stone");
        LanguageRegistry.addName(BlessedStoneSword, "Blessed Stone Sword");
        GameRegistry.addRecipe(new ItemStack(BlessedStoneSword), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Block.stone, 's', Item.stick);

        BlessedWoodSword = new BlessedSword(config.BlessedWoodSword, BlessedWoodTool).setUnlocalizedName("Blessed_Sword_Wood").setTextureName("divine_equipment:Blessed_Sword_Wood");
        LanguageRegistry.addName(BlessedWoodSword, "Blessed Wooden Sword");
        GameRegistry.addRecipe(new ItemStack(BlessedWoodSword), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Block.planks, 's', Item.stick);

        // Pickaxes

        BlessedDiamondPickaxe = new BlessedPickaxe(config.BlessedDiamondPickaxe, BlessedDiamondTool).setUnlocalizedName("Blessed_Pickaxe_Diamond").setTextureName("divine_equipment:Blessed_Pickaxe_Diamond");
        LanguageRegistry.addName(BlessedDiamondPickaxe, "Blessed Diamond Pickaxe");
        GameRegistry.addRecipe(new ItemStack(BlessedDiamondPickaxe), "mSm", " s ", " s ", 'S', BlessedSigil, 'm', Item.diamond, 's', Item.stick);

        BlessedGoldPickaxe = new BlessedPickaxe(config.BlessedGoldPickaxe, BlessedGoldTool).setUnlocalizedName("Blessed_Pickaxe_Gold").setTextureName("divine_equipment:Blessed_Pickaxe_Gold");
        LanguageRegistry.addName(BlessedGoldPickaxe, "Blessed Gold Pickaxe");
        GameRegistry.addRecipe(new ItemStack(BlessedGoldPickaxe), "mSm", " s ", " s ", 'S', BlessedSigil, 'm', Item.ingotGold, 's', Item.stick);

        BlessedIronPickaxe = new BlessedPickaxe(config.BlessedIronPickaxe, BlessedIronTool).setUnlocalizedName("Blessed_Pickaxe_Iron").setTextureName("divine_equipment:Blessed_Pickaxe_Iron");
        LanguageRegistry.addName(BlessedIronPickaxe, "Blessed Iron Pickaxe");
        GameRegistry.addRecipe(new ItemStack(BlessedIronPickaxe), "mSm", " s ", " s ", 'S', BlessedSigil, 'm', Item.ingotIron, 's', Item.stick);

        BlessedStonePickaxe = new BlessedPickaxe(config.BlessedStonePickaxe, BlessedStoneTool).setUnlocalizedName("Blessed_Pickaxe_Stone").setTextureName("divine_equipment:Blessed_Pickaxe_Stone");
        LanguageRegistry.addName(BlessedStonePickaxe, "Blessed Stone Pickaxe");
        GameRegistry.addRecipe(new ItemStack(BlessedStonePickaxe), "mSm", " s ", " s ", 'S', BlessedSigil, 'm', Block.stone, 's', Item.stick);

        BlessedWoodPickaxe = new BlessedPickaxe(config.BlessedWoodPickaxe, BlessedWoodTool).setUnlocalizedName("Blessed_Pickaxe_Wood").setTextureName("divine_equipment:Blessed_Pickaxe_Wood");
        LanguageRegistry.addName(BlessedWoodPickaxe, "Blessed Wooden Pickaxe");
        GameRegistry.addRecipe(new ItemStack(BlessedWoodPickaxe), "mSm", " s ", " s ", 'S', BlessedSigil, 'm', Block.planks, 's', Item.stick);

       // Axes

        BlessedDiamondAxe = new BlessedAxe(config.BlessedDiamondAxe, BlessedDiamondTool).setUnlocalizedName("Blessed_Axe_Diamond").setTextureName("divine_equipment:Blessed_Axe_Diamond");
        LanguageRegistry.addName(BlessedDiamondAxe, "Blessed Diamond Axe");
        GameRegistry.addRecipe(new ItemStack(BlessedDiamondAxe), "mS ", "ms ", " s ", 'S', BlessedSigil, 'm', Item.diamond, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedDiamondAxe), " Sm", " sm", " s ", 'S', BlessedSigil, 'm', Item.diamond, 's', Item.stick);

        BlessedGoldAxe = new BlessedAxe(config.BlessedGoldAxe, BlessedGoldTool).setUnlocalizedName("Blessed_Axe_Gold").setTextureName("divine_equipment:Blessed_Axe_Gold");
        LanguageRegistry.addName(BlessedGoldAxe, "Blessed Gold Axe");
        GameRegistry.addRecipe(new ItemStack(BlessedGoldAxe), "mS ", "ms ", " s ", 'S', BlessedSigil, 'm', Item.ingotGold, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedGoldAxe), " Sm", " sm", " s ", 'S', BlessedSigil, 'm', Item.ingotGold, 's', Item.stick);


        BlessedIronAxe = new BlessedAxe(config.BlessedIronAxe, BlessedIronTool).setUnlocalizedName("Blessed_Axe_Iron").setTextureName("divine_equipment:Blessed_Axe_Iron");
        LanguageRegistry.addName(BlessedIronAxe, "Blessed Iron Axe");
        GameRegistry.addRecipe(new ItemStack(BlessedIronAxe), "mS ", "ms ", " s ", 'S', BlessedSigil, 'm', Item.ingotIron, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedGoldAxe), " Sm", " sm", " s ", 'S', BlessedSigil, 'm', Item.ingotGold, 's', Item.stick);

        BlessedStoneAxe = new BlessedAxe(config.BlessedStoneAxe, BlessedStoneTool).setUnlocalizedName("Blessed_Axe_Stone").setTextureName("divine_equipment:Blessed_Axe_Stone");
        LanguageRegistry.addName(BlessedStoneAxe, "Blessed Stone Axe");
        GameRegistry.addRecipe(new ItemStack(BlessedStoneAxe), "mS ", "ms ", " s ", 'S', BlessedSigil, 'm', Block.stone, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedStoneAxe), " Sm", " sm", " s ", 'S', BlessedSigil, 'm', Block.stone, 's', Item.stick);

        BlessedWoodAxe = new BlessedAxe(config.BlessedWoodAxe, BlessedWoodTool).setUnlocalizedName("Blessed_Axe_Wood").setTextureName("divine_equipment:Blessed_Axe_Wood");
        LanguageRegistry.addName(BlessedWoodAxe, "Blessed Wooden Axe");
        GameRegistry.addRecipe(new ItemStack(BlessedWoodAxe), "mS ", "ms ", " s ", 'S', BlessedSigil, 'm', Block.planks, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedWoodAxe), " Sm", " sm", " s ", 'S', BlessedSigil, 'm', Block.planks, 's', Item.stick);

        // Shovels

        BlessedDiamondShovel = new BlessedShovel(config.BlessedDiamondShovel, BlessedDiamondTool).setUnlocalizedName("Blessed_Shovel_Diamond").setTextureName("divine_equipment:Blessed_Shovel_Diamond");
        LanguageRegistry.addName(BlessedDiamondShovel, "Blessed Diamond Shovel");
        GameRegistry.addRecipe(new ItemStack(BlessedDiamondShovel), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Item.diamond, 's', Item.stick);

        BlessedGoldShovel = new BlessedShovel(config.BlessedGoldShovel, BlessedGoldTool).setUnlocalizedName("Blessed_Shovel_Gold").setTextureName("divine_equipment:Blessed_Shovel_Gold");
        LanguageRegistry.addName(BlessedGoldShovel, "Blessed Gold Shovel");
        GameRegistry.addRecipe(new ItemStack(BlessedGoldShovel), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Item.ingotGold, 's', Item.stick);

        BlessedIronShovel = new BlessedShovel(config.BlessedIronShovel, BlessedIronTool).setUnlocalizedName("Blessed_Shovel_Iron").setTextureName("divine_equipment:Blessed_Shovel_Iron");
        LanguageRegistry.addName(BlessedIronShovel, "Blessed Iron Shovel");
        GameRegistry.addRecipe(new ItemStack(BlessedIronShovel), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Item.ingotIron, 's', Item.stick);

        BlessedStoneShovel = new BlessedShovel(config.BlessedStoneShovel, BlessedStoneTool).setUnlocalizedName("Blessed_Shovel_Stone").setTextureName("divine_equipment:Blessed_Shovel_Stone");
        LanguageRegistry.addName(BlessedStoneShovel, "Blessed Stone Shovel");
        GameRegistry.addRecipe(new ItemStack(BlessedStoneShovel), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Block.stone, 's', Item.stick);

        BlessedWoodShovel = new BlessedShovel(config.BlessedWoodShovel, BlessedWoodTool).setUnlocalizedName("Blessed_Shovel_Wood").setTextureName("divine_equipment:Blessed_Shovel_Wood");
        LanguageRegistry.addName(BlessedWoodShovel, "Blessed Wooden Shovel");
        GameRegistry.addRecipe(new ItemStack(BlessedWoodShovel), " m ", " S ", " s ", 'S', BlessedSigil, 'm', Block.planks, 's', Item.stick);

        // Blessed Hoes

        BlessedDiamondHoe = new BlessedHoe(config.BlessedDiamondHoe, BlessedDiamondTool).setUnlocalizedName("Blessed_Hoe_Diamond").setTextureName("divine_equipment:Blessed_Hoe_Diamond");
        LanguageRegistry.addName(BlessedDiamondHoe, "Blessed Diamond Hoe");
        GameRegistry.addRecipe(new ItemStack(BlessedDiamondHoe), "mS ", " s ", " s ", 'S', BlessedSigil, 'm', Item.diamond, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedDiamondHoe), " Sm", " s ", " s ", 'S', BlessedSigil, 'm', Item.diamond, 's', Item.stick);

        BlessedGoldHoe = new BlessedHoe(config.BlessedGoldHoe, BlessedGoldTool).setUnlocalizedName("Blessed_Hoe_Gold").setTextureName("divine_equipment:Blessed_Hoe_Gold");
        LanguageRegistry.addName(BlessedGoldHoe, "Blessed Gold Hoe");
        GameRegistry.addRecipe(new ItemStack(BlessedGoldHoe), "mS ", " s ", " s ", 'S', BlessedSigil, 'm', Item.ingotGold, 's', Item.stick);

        BlessedIronHoe = new BlessedHoe(config.BlessedIronHoe, BlessedIronTool).setUnlocalizedName("Blessed_Hoe_Iron").setTextureName("divine_equipment:Blessed_Hoe_Iron");
        LanguageRegistry.addName(BlessedIronHoe, "Blessed Iron Hoe");
        GameRegistry.addRecipe(new ItemStack(BlessedIronHoe), "mS ", " s ", " s ", 'S', BlessedSigil, 'm', Item.ingotIron, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedGoldHoe), " Sm", " s ", " s ", 'S', BlessedSigil, 'm', Item.ingotGold, 's', Item.stick);

        BlessedStoneHoe = new BlessedHoe(config.BlessedStoneHoe, BlessedStoneTool).setUnlocalizedName("Blessed_Hoe_Stone").setTextureName("divine_equipment:Blessed_Hoe_Stone");
        LanguageRegistry.addName(BlessedStoneHoe, "Blessed Stone Hoe");
        GameRegistry.addRecipe(new ItemStack(BlessedStoneHoe), "mS ", " s ", " s ", 'S', BlessedSigil, 'm', Block.stone, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedStoneHoe), " Sm", " s ", " s ", 'S', BlessedSigil, 'm', Block.stone, 's', Item.stick);

        BlessedWoodHoe = new BlessedHoe(config.BlessedWoodHoe, BlessedWoodTool).setUnlocalizedName("Blessed_Hoe_Wood").setTextureName("divine_equipment:Blessed_Hoe_Wood");
        LanguageRegistry.addName(BlessedWoodHoe, "Blessed Wooden Hoe");
        GameRegistry.addRecipe(new ItemStack(BlessedWoodHoe), "mS ", " s ", " s ", 'S', BlessedSigil, 'm', Block.planks, 's', Item.stick);
        GameRegistry.addRecipe(new ItemStack(BlessedWoodHoe), " Sm", " s ", " s ", 'S', BlessedSigil, 'm', Block.planks, 's', Item.stick);

        // Ender Items

        EnderSword = new items.ender.EnderSword(config.EnderSword, EnumToolMaterial.EMERALD).setUnlocalizedName("Ender_Sword").setTextureName("divine_equipment:EnderSword");
        LanguageRegistry.addName(EnderSword, "Ender Sword");
        GameRegistry.addRecipe(new ItemStack(EnderSword), " s ", " s ", " S ", 's', EnderSigil, 'S', Item.stick);
    }
}
