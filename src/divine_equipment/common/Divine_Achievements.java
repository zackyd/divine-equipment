package common;

import net.minecraft.item.Item;
import net.minecraft.stats.Achievement;

/**
 * User: Orthus
 * Date: 10/4/13
 */

public class Divine_Achievements {
    Item BlessedSigil = Divine_Content.BlessedSigil;
    Item BlankSigil = Divine_Content.BlankSigil;
    Item TaintedSigil = Divine_Content.TaintedSigil;
    Item EnderSigil = Divine_Content.EnderSigil;
//    Item BlssedAlter = Divine_Content.BlessedAlter;
//    Item TaintedAlter = Divine_Content.TaintedAlter;
//    Item EnderAlter = Divine_Content.EnderAlter;

    public Divine_Achievements(){
        achievements();
    }

// Achievements
public static Achievement DivineWill;
public static Achievement HollowBeThyName;
public static Achievement ChildOfTheDamned;
public static Achievement EndersGame;
//public static Achievement FallenOne;
//public static Achievement Redemption;
//public static Achievement ShadowWarrior;

void achievements(){
    DivineWill = new Achievement(2500, "Divine Will", 20, 20, BlankSigil, null);
    HollowBeThyName = new Achievement(2501, "Hollow Be Thy Name", 18, 20, BlessedSigil, DivineWill);
    ChildOfTheDamned = new Achievement(2502, "Child Of The Damned", 20, 18, TaintedSigil, DivineWill);
    EndersGame = new Achievement(2503, "Enders Game", 22, 20, EnderSigil, DivineWill);
//    Redemption = new Achievement(2505, "Redemption", 4, 0, BlssedAlter, HollowBeThyName);
//    FallenOne = new Achievement(2504, "Fallon One", 0, -6, TaintedAlter, ChildOfTheDamned);
//    ShadowWarrior = new Achievement(2506, "Shadow Warror", -2, -4, EnderAlter, EndersGame);

}}

