package items.blessed;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemSpade;

/**
 * User: Orthus
 * Date: 10/8/13
 */
public class BlessedShovel extends ItemSpade {
    public BlessedShovel(int par1, EnumToolMaterial par2EnumToolMaterial) {
        super(par1, par2EnumToolMaterial);
        setCreativeTab(CreativeTabs.tabTools);
    }
}
