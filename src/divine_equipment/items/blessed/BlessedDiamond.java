package items.blessed;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * User: Orthus
 * Date: 9/30/13
 */
public class BlessedDiamond extends Item{
        public BlessedDiamond(int par1) {
        super(par1);
        setCreativeTab(CreativeTabs.tabMaterials);
        }
}
